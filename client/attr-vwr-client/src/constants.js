export default {
    department: [{
            "value": "All",
            "label": "All"
        },
        {
            "value": "Clothing, Shoes & Accessories",
            "label": "Clothing, Shoes & Accessories"
        },
        {
            "value": "Home & Garden",
            "label": "Home & Garden"
        },
        {
            "value": "Jewelry, Gems & Watches",
            "label": "Jewelry, Gems & Watches"
        },
        {
            "value": "Books, Music & Movies",
            "label": "Books, Music & Movies"
        },
        {
            "value": "Electronics",
            "label": "Electronics"
        },
        {
            "value": "Art",
            "label": "Art"
        },
        {
            "value": "Tools & Hardware",
            "label": "Tools & Hardware"
        },
        {
            "value": "Vehicles, Parts & Accessories",
            "label": "Vehicles, Parts & Accessories"
        },
        {
            "value": "Sports & Outdoors",
            "label": "Sports & Outdoors"
        },
        {
            "value": "Office & Stationery",
            "label": "Office & Stationery"
        },
        {
            "value": "Health & Beauty",
            "label": "Health & Beauty"
        },
        {
            "value": "Crafts",
            "label": "Crafts"
        },
        {
            "value": "Toys & Games",
            "label": "Toys & Games"
        },
        {
            "value": "Food & Beverages",
            "label": "Food & Beverages"
        },
        {
            "value": "Pet Supplies",
            "label": "Pet Supplies"
        },
        {
            "value": "Cameras, Photography & Optics",
            "label": "Cameras, Photography & Optics"
        },
        {
            "value": "Baby",
            "label": "Baby"
        },
        {
            "value": "Business & Industrial",
            "label": "Business & Industrial"
        },
        {
            "value": "Collectibles & Memorabilia",
            "label": "Collectibles & Memorabilia"
        },
        {
            "value": "Musical Instruments & Pro Audio",
            "label": "Musical Instruments & Pro Audio"
        },
        {
            "value": "Travel, Luggage & Accessories",
            "label": "Travel, Luggage & Accessories"
        },
        {
            "value": "Services & Warranties",
            "label": "Services & Warranties"
        },
        {
            "value": "Everything Else",
            "label": "Everything Else"
        },
        {
            "value": "Gift Cards & Gift Certificates",
            "label": "Gift Cards & Gift Certificates"
        },
        {
            "value": "Hand Tools",
            "label": "Hand Tools"
        },
        {
            "value": "Tickets",
            "label": "Tickets"
        }
    ],
    curated_level: [
        { "value": "All", "label": "All" },
        { "value": "-1", "label": "-1" },
        { "value": "0", "label": "0" },
        { "value": "1", "label": "1" },
        { "value": "2", "label": "2" },
        { "value": "3", "label": "3" },
    ]
}