import React, { Component } from 'react';
import axios from 'axios'
// import debounce from 'lodash.debounce'

// import WordCloud from 'react-d3-cloud';
import Select from 'react-select';
import ReactTable from 'react-table';
import checkboxHOC from 'react-table/lib/hoc/selectTable';
import { RingLoader } from 'react-spinners';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { HotKeys } from 'react-hotkeys';

// import logo from './logo.svg';
import './App.css';
import 'react-notifications/lib/notifications.css';
import 'react-select/dist/react-select.css';
import 'react-table/react-table.css';
import Chance from 'chance';

import constants from './constants'

const HOST = process.env.API_SERVER || '174.138.39.108'
const PORT = process.env.API_SERVER_PORT || '5000'
const nbr_format = new Intl.NumberFormat('en-US', { style: 'decimal', minimumFractionDigits:0 })

const CheckboxTable = checkboxHOC(ReactTable);
const chance = new Chance();

String.prototype.toProperCase = function () {
  return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

const data = [
  { text: 'Select', value: 1000 },
  { text: 'department/ product type/ curation level', value: 800 },
  { text: 'to make', value: 600 },
  { text: 'attribute', value: 500 },
  { text: 'cloud', value: 10 },
];

const keyMap = {
  'save': { sequence:['command+s', 'ctrl+s'], action:'keydown'}
};

const all_opts = {
  value: 'All',
  label: 'All'
}

// const fontSizeMapper = word => Math.log(word.value) * 5;
// const rotate = 0;

const Filters = props => {
  return (
    <table className='av-table'><tbody>
      <tr className='av-select'><td>Select Department</td><td>Select Product Type</td><td>Select Curation Level</td></tr>
      <tr className='av-select'><td><Select
        key='department'
        name='department'
        value={props.department}
        onChange={props.handleDeptChange}
        options={constants.department}
        focusedOption={all_opts}
        resetValue={all_opts}
      /></td>
        <td><Select
          key='product_type'
          name='product_type'
          value={props.product_type}
          onChange={props.handleProductTypeChange}
          options={props.product_type_values}
          focusedOption={all_opts}
          resetValue={all_opts}
        /></td>
      <td><Select 
          key='curated_level'
          name='curated_level'
          value={props.curated_level}
          onChange={props.handleCuratedLevelChange}
          options={constants.curated_level}
          focusedOption={'All'}
          resetValue={'All'}
        /></td></tr>
    </tbody></table>
  )
}

class App extends Component {
  state = {
    department: all_opts,
    product_type: all_opts,
    curated_level: all_opts,
    product_type_values: [all_opts],
    data: data.map(d => {return {_id: chance.guid(), ...d}}),
    columns: [{ 
      Header: 'Attribute Name', 
      accessor: 'text' , 
      Cell: r => <span>{ r.value.replace(/_/g," ").toProperCase() } - <code style={{color:'gray'}}>{r.value}</code></span>,
      filterMethod: (filter, row) => row[filter.id].match(new RegExp(filter.value, 'i'))
    },{ 
      Header: 'Count of Product Records', 
      accessor: 'value', Cell: r => nbr_format.format(r.value), 
      maxWidth:400,
      filterable: false
    }],
    isFetching: false,
    // ascCount: false,
    // ascName: false,
    // sort_column: 'count',
    selection: [],
    selectAll: false,
    sessionId: null,
  }

  // componentWillMount() {
  //   this.handleKeyPress = debounce(this.handleKeyPress, 420)
  // }

  componentDidMount() {
    if(localStorage !== undefined) {
      let sessionId = localStorage.getItem('avc-session-id')
      if(!sessionId) 
        sessionId = chance.guid()
      this.setState({sessionId})
      localStorage.setItem('avc-session-id',sessionId)
    }
  }

  getAttributes(department, product_type, curated_level, callback) {
    axios.post(`http://${HOST}:${PORT}/get_attributes`, {
      department,
      product_type,
      curated_level
    }, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
          'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
        }
    }).then(response => {
      callback(null, {
        data: response.data.map(d => {return {_id:chance.guid(), ...d}}), 
        isFetching: false,
      })
    }).catch(e => {
      callback(e)
    })
  } 

  handleCuratedLevelChange = (cl) => {
    this.setState({ 
      curated_level: cl, 
      isFetching: true 
    })

    this.getAttributes(
      this.state.department.value || all_opts.value, 
      this.state.product_type.value || all_opts.value, 
      cl.value,
      (err, res) => {
        if(!err) this.setState(res)
        else console.log(err)
    })
  }

  handleDeptChange = (dept) => {
    if ((dept.value === 'All' && this.state.department.value === dept.value) || dept === undefined) {
      this.setState({ department: dept || all_opts, product_type: all_opts })
      return
    }

    this.setState({ department: dept, isFetching: true })
    axios.get(`http://${HOST}:${PORT}/api/get_products/` + dept.value, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
      }
    }).then(response => {
      this.setState({
        product_type_values: [all_opts, ...response.data],
        product_type: all_opts
      })

      this.getAttributes(
        dept.value, 
        'All', 
        this.state.curated_level.value, 
        (err, res) => {
          if(!err) this.setState(res)
          else console.log(err)
      })

    }).catch(e => {
      console.log(e)
    })
  }

  handleProductTypeChange = (pt) => {
    this.setState({ 
      product_type: pt, 
      isFetching: true 
    })

    this.getAttributes(
      this.state.department.value, 
      pt.value, 
      this.state.curated_level.value,
      (err, res) => {
        if(!err) this.setState(res)
        else console.log(err)
    })

  }

  // onWordClick = d => {
  //   NotificationManager.info(nbr_format.format(d.value), d.text)
  // }

  // setSortColumn = col => {
  //   this.setState({
  //     sort_column: col,
  //     ascCount: col === 'count' ?  !this.state.ascCount : this.state.ascCount,
  //     ascName: col === 'name' ? !this.state.ascName: this.state.ascName,
  //   })
  // }

  // handleKeyPress = text => {
  //   this.setState({ filterNameQuery: text })
  // }

  toggleSelection = (key, shift, row) => {
    /*
      Implementation of how to manage the selection state is up to the developer.
      This implementation uses an array stored in the component state.
      Other implementations could use object keys, a Javascript Set, or Redux... etc.
    */
    // start off with the existing state
    let selection = [
      ...this.state.selection
    ];
    const keyIndex = selection.indexOf(key);
    // check to see if the key exists
    if (keyIndex >= 0) {
      // it does exist so we will remove it using destructing
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ]
    } else {
      // it does not exist so add it
      selection.push(key);
    }
    // update the state
    this.setState({ selection });
  }

  toggleAll = () => {
    /*
      'toggleAll' is a tricky concept with any filterable table
      do you just select ALL the records that are in your data?
      OR
      do you only select ALL the records that are in the current filtered data?
      
      The latter makes more sense because 'selection' is a visual thing for the user.
      This is especially true if you are going to implement a set of external functions
      that act on the selected information (you would not want to DELETE the wrong thing!).
      
      So, to that end, access to the internals of ReactTable are required to get what is
      currently visible in the table (either on the current page or any other page).
      
      The HOC provides a method call 'getWrappedInstance' to get a ref to the wrapped
      ReactTable and then get the internal state and the 'sortedData'. 
      That can then be iterrated to get all the currently visible records and set
      the selection state.
    */
    const selectAll = this.state.selectAll ? false : true;
    const selection = [];
    if (selectAll) {
      // we need to get at the internals of ReactTable
      const wrappedInstance = this.checkboxTable.getWrappedInstance();
      // the 'sortedData' property contains the currently accessible records based on the filter and sort
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      // we just push all the IDs onto the selection array
      currentRecords.forEach((item) => {
        selection.push(item._original._id);
      })
    }
    this.setState({ selectAll, selection })
  }

  isSelected = (key) => {
    /*
      Instead of passing our external selection state we provide an 'isSelected'
      callback and detect the selection state ourselves. This allows any implementation
      for selection (either an array, object keys, or even a Javascript Set object).
    */
    return this.state.selection.includes(key);
  }

  logSelection = () => {
    // console.log('selection:', this.state.selection);
    const dt = new Date()
    NotificationManager.info(`Starting save at ${dt}! You can continue to work while it saves.`, 'Save Curation')
    
    axios.post(`http://${HOST}:${PORT}/saveCuration`, {
      department: this.state.department.value,
      product_type: this.state.product_type.value,
      curated_level: this.state.curated_level.value,
      sessionId: this.state.sessionId,
      data: this.state.data.map(d => {
        return {
          ...d,
          isGood: this.isSelected(d._id)
        }
      })
    }).then(res => {
      NotificationManager.success(`Save initiated at ${dt} done!`)
    }).catch(err => {
      NotificationManager.error(`Save initiated at ${dt} errored out!`)
    })

  }

  handlers = {
    'save': event => {event.stopPropagation(); event.preventDefault(); this.logSelection();}
  }
  
  render() {
    const { toggleSelection, toggleAll, isSelected, logSelection, } = this;
    const { columns, selectAll, } = this.state;

    const checkboxProps = {
      selectAll,
      isSelected,
      toggleSelection,
      toggleAll,
      selectType: 'checkbox',
    };

    return (
      <HotKeys keyMap={keyMap} handlers={this.handlers}>
      <div className="App">
        { /* <header className = "App-header">
          <img src={ logo } className="App-logo" alt="logo"/>
          <h1 className = "App-title"> 
            Welcome to React 
          </h1>
        </header > 
    */ }
        <div className="App-intro">
          <Filters
            department={this.state.department}
            product_type={this.state.product_type}
            curated_level={this.state.curated_level}
            product_type_values={this.state.product_type_values}
            handleCuratedLevelChange={this.handleCuratedLevelChange}
            handleDeptChange={this.handleDeptChange}
            handleProductTypeChange={this.handleProductTypeChange}
          />
          {
            this.state.isFetching ?
              <div className='av-center'>
                <RingLoader
                  color={'#123abc'}
                  loading={this.state.isFetching}
                />
              </div> :
              <div className='av-wordcloud-table'>
                { /* <WordCloud
                  data={this.state.data}
                  fontSizeMapper={fontSizeMapper}
                  rotate={rotate}
                  onWordClick={this.onWordClick}
                /> */ }
                <div className='av-tableview'>
                  {/*<TableView 
                    department={this.state.department.label}
                    product_type={this.state.product_type.label}
                    curated_level={this.state.curated_level.label}
                    data={this.state.data}
                    sort_column={this.state.sort_column}
                    ascCount={this.state.ascCount}
                    ascName={this.state.ascName}
                    filterNameQuery={this.state.filterNameQuery}
                    setSortColumn={this.setSortColumn}
                    handleKeyPress={this.handleKeyPress}
                  />*/}
                  <button style={{padding:'10px', fontSize:'1em', borderRadius:"3px", border:"1px solid lightgray"}} onClick={logSelection}>
                  Save (<code style={{color:'darkred'}}>Ctrl + s/Cmd + s</code>) Selected Attributes as Good for<p><em>{ `${this.state.department.label} > ${this.state.product_type.label} > ${this.state.curated_level.label}` }</em></p> </button>
                  <p/>
                  <CheckboxTable
                    ref={(r)=>this.checkboxTable=r}
                    data={this.state.data}
                    columns={columns}
                    defaultPageSize={200}
                    className="-striped -highlight"
                    pageSizeOptions={[100,200,500,1000]}
                    defaultSorted={[{id:'value', desc:true}]}
                    filterable
                    defaultSortDesc={false}
                    {...checkboxProps}
                  />
                  {/*<ReactTable 
                    data={this.state.data}
                    filterable
                    columns={[
                      { 
                        id: 'srno', 
                        Header: 'Sr No', 
                        Cell: r => r.index + 1, 
                        maxWidth: 80,
                        filterable: false
                      },{ 
                        Header: 'Attribute Name', 
                        accessor: 'text' , 
                        Cell: r => <span>{ r.value.replace(/_/g," ").toProperCase() } - <code style={{color:'gray'}}>{r.value}</code></span>,
                        filterMethod: (filter, row) => row[filter.id].match(new RegExp(filter.value, 'i'))
                      },{ 
                        Header: 'Count of Product Records', 
                        accessor: 'value', Cell: r => nbr_format.format(r.value), 
                        maxWidth:400,
                        filterable: false
                      }
                    ]}
                    defaultPageSize={200}
                    pageSizeOptions={[100,200,500,1000]}
                    defaultSorted={[{id:'value', desc:true}]}
                    defaultSortDesc={true}
                    className='-striped -highlight'
                  />*/}
                  <br/>
                </div>
              </div>
          }
        </div>
        <NotificationContainer />
      </div>
      </HotKeys>
    );
  }
}

// const TableView = props => {
//   const sort_column = props.sort_column || 'count'
//   let tabledata;
//   switch(sort_column) {
//     case 'name':
//       tabledata = props.data.sort((d1, d2) => {
//         if(props.ascName)
//           return d1.text.toLowerCase() > d2.text.toLowerCase() ? 1 : -1
//         else
//           return d1.text.toLowerCase() > d2.text.toLowerCase() ? -1 : 1
//       })
//       break
//     case 'count':
//     default:
//       tabledata = props.data.sort((d1,d2) => Math.sign(!props.ascCount ? d2.value - d1.value: d1.value - d2.value))
//       break
//   }

//   if(props.filterNameQuery !== '') {
//     const re = new RegExp(props.filterNameQuery, 'i')
//     tabledata = tabledata.filter(d => d.text.match(re))
//   }

//   return <table>
//     <thead>
//       <tr>
//         <th colSpan='4'>
//           Department: {props.department} > Product Type: {props.product_type} > Curated Level: {props.curated_level}
//         </th>
//       </tr>
//       <tr>
//         <th>
//           Sr. No
//         </th>
//         <th onClick={() => {props.setSortColumn('name')}}>
//           Attribute Name <code style={{color:'darkgray'}}>⬆</code><code style={{color:'darkgray'}}>⬇</code>
//         </th>
//         <th onClick={() => {props.setSortColumn('count')}}>
//           Count of Product Records <code style={{color:'darkgray'}}>⬆</code><code style={{color:'darkgray'}}>⬇</code>
//         </th>
//         <th>
//           Good Attribute?
//         </th>
//       </tr>
//       <tr>
//         <th>&nbsp;</th>
//         <th>
//           <input className='av-filter-input' type='text' onKeyDown={e => props.handleKeyPress(e.target.value)} placeholder='type to filter attribute names'/>
//         </th>
//         <th>&nbsp;</th>
//         <th>&nbsp;</th>
//       </tr>
//     </thead>
//     <tbody>
//       { 
//         tabledata.map((d,n) => <AttributeRow key={d.text} rowdata={d} srno={n} />) 
//       }
//     </tbody>
//     </table>
// }

// const AttributeRow = props => {
//   return <tr>
//     <td>
//       {props.srno + 1}
//     </td>
//     <td>
//       {props.rowdata.text.replace(/_/g," ").toProperCase()} - <code style={{color:'gray'}}>{props.rowdata.text}</code>
//     </td>
//     <td>
//       {nbr_format.format(props.rowdata.value)}
//     </td>
//     <td>
//       <input type="checkbox" />
//     </td>
//   </tr>
// }

export default App;

