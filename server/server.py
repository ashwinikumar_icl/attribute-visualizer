import json
from collections import Counter
from functools import reduce
from flask import Flask, request
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

with open('./dept_pt_map.json') as f:
    dept_pt_map = json.loads(f.read())

with open('./product_type_root_segment_map.json') as f:
    pidrsmap = json.loads(f.read())

with open('./attribute_count_by_product_type.json') as f:
    attr_ctr = json.loads(f.read())

def do_reduce(acc, val):
    acc.update(val)
    return acc

@app.route('/api/get_products/<department>')
def get_products(department):
    if department == 'All':
        return json.dumps([{
            'value': 'All',
            'label': 'All'
        }])
    else:
        return json.dumps(dept_pt_map[department])

@app.route('/get_attributes', methods=['POST'])
def get_attributes():
    print(json.loads(request.data.decode('utf-8')))
    data = json.loads(request.data.decode('utf-8'))
    department = data['department']
    product_type = data['product_type']
    curated_level = data['curated_level']

    if(department == 'All'):
        return json.dumps(
            [
                {
                    'text':k, 
                    'value':v
                } for k, v in reduce(
                    do_reduce, 
                    [
                        Counter(v1) for k1, v1 in attr_ctr.items() if curated_level == "All" or k1.split('^^^')[-1] == curated_level
                    ],
                    Counter()
                ).items()
            ]
        )
    elif(product_type == 'All'):
        return json.dumps(
            [
                {
                    'text':k.split('^^^')[0], 
                    'value':v
                } for k, v in reduce(
                    do_reduce, 
                    [
                        Counter(v1) for k1, v1 in attr_ctr.items() 
                        if k1.split("^^^")[0] in pidrsmap and 
                            (
                                curated_level == 'All' or 
                                k1.split('^^^')[-1] == curated_level
                            ) and
                            pidrsmap[
                                k1.split("^^^")[0]
                            ] == department
                    ], 
                    Counter()
                ).items()
            ]
        )
    else:
        return json.dumps(
            [
                {
                    'text':k.split('^^^')[0], 
                    'value':v
                } for k, v in reduce(
                    do_reduce, 
                    [
                        Counter(v1) for k1, v1 in attr_ctr.items() 
                        if k1.split('^^^')[0] == product_type and
                            (
                                curated_level == 'All' or
                                k1.split('^^^')[-1] == curated_level
                            )
                    ], 
                    Counter()
                ).items()
            ]
        )

@app.route('/saveCuration', methods=['POST'])
def saveCuration():
    data = json.loads(request.data.decode('utf-8'))
    department = data['department']
    product_type = data['product_type']
    curated_level = data['curated_level']
    sessionId = data['sessionId']
    product_records = data['data']

    lines = []
    for line in open('./curated_output.json'):
        jl = json.loads(line.strip('\n'))
        if jl['department'] == department and \
            jl['product_type'] == product_type and \
            jl['curated_level'] == curated_level and \
            jl['sessionId'] == sessionId:
            pass
        else:
            lines.append(line)

    data['data'] = list(map(lambda x: { 'text': x['text'], 'isGood': x['isGood'], 'value': x['value'] }, data['data']))
    lines.append(json.dumps(data) + '\n')
    
    with open('./curated_output.json', 'a') as f:
        f.write("".join(lines))

    return 'success'